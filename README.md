# Tanuki Racing Application

Main project for all GitLab hands on events, main project here: https://gitlab.com/gitlab-learn-labs/sample-projects/tanuki-racing/-/edit/main/README.md.

**GitLab Duo/AI** - This workshop focuses on our Generative AI portfolio of features currently offered on GitLab SaaS. In this workshop, participants will get hands-on with GitLab Duo features such as Suggested Reviewers, Code Suggestions, Duo Chat, and Summarize/Resolve this Vulnerability. This workshop is designed to introduce participants to our approach to Generative AI and improve their understanding of how they can utilize our Duo features in their daily workflows.

[Deck | Google Slides](https://docs.google.com/presentation/d/1Ybg10UkrVT_L74nHT5GpCmkkm8AICAEDEpEwtqbEtVI/edit#slide=id.g24acd83f362_0_580)\
[Lab | GitLab Repo](https://gitlab.com/gitlab-learn-labs/sample-projects/tanuki-racing/-/tree/main/Courses/Workshops/AI?ref_type=heads) 

## Contribution Guidlines

Tanuki Racing is actively maintained by the Demo Architecture and GitLab team. To get started, please click **Request Access** at the top of the page to start contributing. Feel free to contribute back to the project by opening a merge request to fix a broken pipeline, piece of code, or add an additional course. Please remeber to assign the MR to @lfstucker and add an entry to the RELEASE.md to keep track of any changes or updates. **Please note you must be a GitLab employee to contribute to the project**
